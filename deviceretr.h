#ifndef DEVICERETR_H
#define DEVICERETR_H

#include "devicebase.h"

#include <QHostAddress>

class DeviceRetr : public DeviceBase
{
public:
    DeviceRetr(QHostAddress addr);

    virtual void parseSetts(QString str, Ui::MainWindow *ui)override;
    virtual QString encodeSetts(Ui::MainWindow *ui)override;
};

#endif // DEVICERETR_H
