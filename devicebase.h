#ifndef DEVICEBASE_H
#define DEVICEBASE_H

#include <QObject>
#include <QString>
#include <QMainWindow>
#include <QHostAddress>
#include "mainwindow.h"

namespace Ui { class MainWindow; }

class DeviceBase : public QObject
{
    Q_OBJECT

public:
    DeviceBase(QHostAddress addr);
    virtual ~DeviceBase();

    virtual void parseSetts(QString str, Ui::MainWindow *ui) = 0;
    virtual QString encodeSetts(Ui::MainWindow *ui) = 0;

private:
    QHostAddress address;
};

#endif // DEVICEBASE_H
