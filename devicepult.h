#ifndef DEVICEPULT_H
#define DEVICEPULT_H

#include "devicebase.h"

class DevicePult : public DeviceBase
{
public:
    DevicePult(QHostAddress addr);

    virtual void parseSetts(QString str, Ui::MainWindow *ui)override;
    virtual QString encodeSetts(Ui::MainWindow *ui)override;
};

#endif // DEVICEPULT_H
