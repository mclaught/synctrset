#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>

#include "devicebase.h"
#include "json.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

using namespace nlohmann;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btFind_clicked();

    void readUDP();
    void on_btLoad_clicked();

    void on_btSave_clicked();

    void on_cbHost_activated(const QString &arg1);

    void on_btPairStart_clicked();

    void on_btPairStop_clicked();

    void on_btAddLang_triggered(QAction *arg1);

    void on_btAddLang_clicked();

    void on_btDelLang_clicked();

private:
    Ui::MainWindow *ui;
    QUdpSocket udp;
    QSet<QHostAddress> addrs;
    json js;
    json jLngs;
    QList<DeviceBase*> devices;
    json findLangByDescr(QString descr);
    json findLangByPort(int port);
};
#endif // MAINWINDOW_H
