#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QUdpSocket>
#include "QMessageBox"
#include <string>
#include <strstream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <DeviceRetr.h>
#include <DevicePult.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    foreach(QNetworkInterface iface, QNetworkInterface::allInterfaces()){
        if(iface.IsRunning && iface.isValid() && iface.type() == QNetworkInterface::Ethernet && (iface.flags() & QNetworkInterface::CanBroadcast)){
            qDebug() << iface.humanReadableName();

            foreach(QHostAddress addr, iface.allAddresses()){
                if(!addr.isLoopback() && addr.toIPv4Address()){
                    for(int i=0; i<3; i++){
                        uint32_t mask = pow(2, 8*(i+1))-1;
                        addr.setAddress(addr.toIPv4Address() | mask);

                        addrs.insert(addr);

                        qDebug() << "     " << addr;
                    }
                }
            }
        }
    }

    udp.bind(QHostAddress::AnyIPv4, 2001);
    connect(&udp, &QIODevice::readyRead, this, &MainWindow::readUDP);

    std::ifstream fLngs;
    fLngs.open("langs.json");
    if(fLngs.is_open()){
        try{
            jLngs = json::parse(fLngs);//, nullptr, false
            if(jLngs.is_array()){
                for(json jL : jLngs){
                    if(jL.is_object()){
                        int port = jL["port"];
                        std::string descr = jL["descr"];
                        ui->lstLangs->addItem(QString("%1").arg(descr.c_str()));
                    }
                }
            }
        }catch(json::exception& e){
            qDebug() << e.what();
        }
    }

    ui->cbBitrate->addItem("16000");
    ui->cbBitrate->addItem("24000");
    ui->cbBitrate->addItem("32000");
    ui->cbBitrate->addItem("40000");

    on_btFind_clicked();
}

MainWindow::~MainWindow()
{
    delete ui;
}

json MainWindow::findLangByDescr(QString descr){
    if(!jLngs.is_array())
        return json::object();

    for(json j : jLngs){
        std::string tstDescr = j["descr"];
        if(j.is_object() && descr.toStdString()==tstDescr){
            return j;
        }
    }
    return json::object();
}

json MainWindow::findLangByPort(int port){
    if(!jLngs.is_array())
        return json::object();

    for(json j : jLngs){
        int tstPort = j["port"];
        if(j.is_object() && port==tstPort){
            return j;
        }
    }
    return json::object();
}
void MainWindow::readUDP(){
    char buf[1024];
    QHostAddress host;
    auto sz = udp.readDatagram(buf, 1024, &host);
    buf[sz] = 0;

    QString str = QString(buf);
    qDebug() << str;

    if(str.left(7) == "IAMHERE"){
        qDebug() << "Recv from " << host;
        if(ui->cbHost->findText(host.toString()) != -1){
            int n = ui->cbHost->count();
            ui->cbHost->addItem(host.toString());
            if(n == 0){
                on_cbHost_activated(host.toString());
            }
            DeviceBase* dev = str.contains("PULT") ?
                        dynamic_cast<DeviceBase*>(new DevicePult(host)) :
                        dynamic_cast<DeviceBase*>(new DeviceRetr(host));
            devices.push_back(dev);
        }
    }else if(str.left(11) == "SETTS_SAVED"){
        QMessageBox::information(this, "Сохранение", "Параметры сохранены");
    }else if(str.left(11) == "SETTS_ERROR"){
        str.remove(0,12);
        QMessageBox::warning(this, "Ошибка", str);
    }else{
        js = json::parse(str.toStdString());
        if(js.is_object()){
            if(js.contains("iface") && js["iface"].is_string())
                ui->edIface->setText(std::string(js["iface"]).c_str());
            if(js.contains("uart_port") && js["uart_port"].is_string())
                ui->edCOMPort->setText(std::string(js["uart_port"]).c_str());
            if(js.contains("remote_addr") && js["remote_addr"].is_string())
                ui->edRemoteAddr->setText(std::string(js["remote_addr"]).c_str());
            if(js.contains("remote_port") && js["remote_port"].is_number_integer())
                ui->edRemotePort->setText(QString("%1").arg(static_cast<int>(js["remote_port"])));
            if(js.contains("session") && js["session"].is_number_integer())
                ui->sbSession->setValue(js["session"]);
            else
                ui->sbSession->setValue(0);
//            if(js.contains("stun_addr") && js["stun_addr"].is_string())
//                ui->edMySTUN->setText(std::string(js["stun_addr"]).c_str());
            if(js.contains("channel"))
                ui->spCh->setValue(js["channel"]);
//            if(js.contains("master_id"))
//                ui->edMasterID->setText(QString("%1").arg(static_cast<int>(js["master_id"])));
            if(js.contains("langs") && js["langs"].is_array()){
                for(json j : js["langs"]){
                    if(j.is_object() && j.contains("descr")){
                        std::string descr = j["descr"];
                        ui->lsSelLngs->addItem(QString(descr.c_str()));
                    }
                }
            }
            if(js.contains("bitrate") && js["bitrate"].is_number()){
                ui->cbBitrate->setCurrentText(QString("%1").arg((int)js["bitrate"]));
            }
        }
    }
}

void MainWindow::on_btFind_clicked()
{
    ui->cbHost->clear();

    foreach(DeviceBase* dev, devices){
        dev->deleteLater();
    }
    devices.clear();

    foreach(QHostAddress addr, addrs){
        qDebug() << "Send to " << addr;
        udp.writeDatagram(QByteArray("DISCOVERY"), addr, 2000);
    }
}

void MainWindow::on_btLoad_clicked()
{
    on_cbHost_activated(ui->cbHost->currentText());
}

void MainWindow::on_btSave_clicked()
{
    if(!js.is_object()){
        js = json::object();
    }

//    js["uart_port"] = ui->edCOMPort->text().toStdString();
    js["remote_addr"] = ui->edRemoteAddr->text().toStdString();
    js["remote_port"] = ui->edRemotePort->text().toInt();
    js["session"] = ui->sbSession->value();
//    js["stun_addr"] = ui->edMySTUN->text().toStdString();
    js["channel"] = ui->spCh->value();
//    js["master_id"] = ui->edMasterID->text().toInt();


    json j_langs = json::array();
    for(int i=0; i<ui->lsSelLngs->count(); i++){
        QString descr = ui->lsSelLngs->item(i)->text();
        json j = findLangByDescr(descr);
        j_langs[i] = j;
    }
    js["langs"] = j_langs;

    js["bitrate"] = ui->cbBitrate->currentText().toInt();

    std::ostrstream stm;
    stm.clear();
    stm << "SETSETTS" << js;// << std::setw(2) << std::setfill(' ')

    std::string str = std::string("SETSETTS") + js.dump();//stm.str()
    int len = str.length();
    udp.writeDatagram(QByteArray(str.c_str(), str.length()), QHostAddress(ui->cbHost->currentText()), 2000);
}

void MainWindow::on_cbHost_activated(const QString &arg1)
{
    qDebug() << "activated" << arg1;
    ui->lsSelLngs->clear();
    udp.writeDatagram(QByteArray("GETSETTS"), QHostAddress(arg1), 2000);//ui->cbHost->currentText()
}

void MainWindow::on_btPairStart_clicked()
{
    std::string str = "PAIRING_START";
    udp.writeDatagram(QByteArray(str.c_str(), str.length()), QHostAddress(ui->cbHost->currentText()), 2000);
}

void MainWindow::on_btPairStop_clicked()
{
    std::string str = "PAIRING_STOP";
    udp.writeDatagram(QByteArray(str.c_str(), str.length()), QHostAddress(ui->cbHost->currentText()), 2000);
}

void MainWindow::on_btAddLang_triggered(QAction *arg1)
{
}

void MainWindow::on_btAddLang_clicked()
{
    QStringList lst = QStringList();
    for(QListWidgetItem* itm : ui->lstLangs->selectedItems()){
        auto found = ui->lsSelLngs->findItems(itm->text(), Qt::MatchExactly);
        int cnt = ui->lsSelLngs->count() + lst.size();
        if(found.count() == 0 && cnt < 8 )
            lst.push_back(itm->text());
    }
    ui->lsSelLngs->addItems(lst);
}

void MainWindow::on_btDelLang_clicked()
{
    for(int i=ui->lsSelLngs->count()-1; i>=0; i--){
        auto itm = ui->lsSelLngs->item(i);
        if(ui->lsSelLngs->isItemSelected(itm)){
            ui->lsSelLngs->takeItem(i);
        }
    }
}
